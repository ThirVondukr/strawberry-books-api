"""Add book table

Revision ID: dd8565c590d1
Revises: 2c19da657cea
Create Date: 2022-02-28 03:56:59.066848

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = "dd8565c590d1"
down_revision = "2c19da657cea"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "book",
        sa.Column("isbn13", sa.String(length=13), nullable=False),
        sa.Column("title", sa.String(length=250), nullable=False),
        sa.PrimaryKeyConstraint("isbn13"),
    )


def downgrade():
    op.drop_table("book")

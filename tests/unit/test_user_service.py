from core.services import UserService


async def test_username_taken(user_service: UserService):
    assert not await user_service.username_taken("Username")


async def test_user_create(user_service: UserService):
    username = "Username"
    password = "Password"
    user = await user_service.user_create(username=username, password=password)
    assert user.username == username
    assert user.password_hash != password
    assert user_service.crypt_context.verify(password, user.password_hash)

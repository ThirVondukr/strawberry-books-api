import time
import uuid

import jwt
import pytest

from core.services import AuthService, UserService
from db.models import User
from settings import AuthSettings


@pytest.fixture
def password() -> str:
    return str(uuid.uuid4())


@pytest.fixture
async def user(user_service: UserService, password: str):
    return await user_service.user_create(
        username=str(uuid.uuid4()), password=password
    )


async def test_authenticate_user(
    auth_service: AuthService, user: User, password: str
):
    user = await auth_service.authenticate(user.username, password)
    assert isinstance(user, User)


async def test_authenticate_user_incorrect_username(
    auth_service: AuthService, user: User, password: str
):
    user = await auth_service.authenticate(user.username + " ", password)
    assert user is None


async def test_authenticate_user_incorrect_password(
    auth_service: AuthService, user: User, password: str
):
    user = await auth_service.authenticate(user.username, password + " ")
    assert user is None


async def test_issue_token(
    user: User,
    auth_service: AuthService,
    auth_settings: AuthSettings,
):
    token = await auth_service.issue_token(user)
    payload = jwt.decode(
        token,
        key=auth_settings.secret_key,
        algorithms=[auth_settings.algorithm],
    )
    expected_exp = (
        payload["iat"] + auth_settings.access_token_lifetime.total_seconds()
    )
    assert payload["exp"] == expected_exp
    assert payload["iat"] == pytest.approx(int(time.time()), rel=1)
    assert payload["sub"] == str(user.id)


async def test_can_authenticate_token(
    auth_service: AuthService,
    user: User,
):
    token = await auth_service.issue_token(user)
    authenticated_user = await auth_service.authenticate_token(token)
    assert user is authenticated_user


async def test_authenticate_token_invalid(
    auth_service: AuthService,
    user: User,
    auth_settings: AuthSettings,
):
    expired_token = jwt.encode(
        payload={
            "sub": str(user.id),
            "iat": time.time() - 60,
            "exp": time.time() - 30,
        },
        key=auth_settings.secret_key,
    )
    assert await auth_service.authenticate_token(expired_token) is None

    token_with_no_user = jwt.encode(
        payload={
            "sub": str(uuid.uuid4()),
            "iat": time.time() - 60,
            "exp": time.time() - 30,
        },
        key=auth_settings.secret_key,
    )
    assert await auth_service.authenticate_token(token_with_no_user) is None

    token_with_different_secret_key = jwt.encode(
        payload={
            "sub": str(user.id),
            "iat": time.time(),
            "exp": time.time() + 3600,
        },
        key="DifferentKey",
    )
    assert (
        await auth_service.authenticate_token(token_with_different_secret_key)
        is None
    )

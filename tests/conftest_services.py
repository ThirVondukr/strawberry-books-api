import pytest
from aioinject import Container, Object
from passlib.context import CryptContext
from sqlalchemy.ext.asyncio import AsyncSession

from core.services import AuthService, UserService
from settings import AuthSettings


@pytest.fixture(scope="session")
async def crypt_context():
    return CryptContext(schemes=["argon2"])


@pytest.fixture
def user_service(
    session: AsyncSession, crypt_context: CryptContext
) -> UserService:
    return UserService(session=session, crypt_context=crypt_context)


@pytest.fixture(scope="session", autouse=True)
def auth_settings(container: Container) -> AuthSettings:
    settings = AuthSettings(secret_key="SecretKey")
    with container.override(Object(settings)):
        yield settings


@pytest.fixture
def auth_service(
    session: AsyncSession,
    user_service: UserService,
    crypt_context: CryptContext,
    auth_settings: AuthSettings,
) -> AuthService:
    return AuthService(
        session=session,
        user_service=user_service,
        crypt_context=crypt_context,
        settings=auth_settings,
    )

from httpx import AsyncClient

from db.models import User

QUERY = """
query Me {
  me {
    __typename
    id
    username
  }
}
"""


async def test_authenticated(user_http_client: AsyncClient, user: User):
    response = await user_http_client.post(
        "/graphql/",
        json={
            "query": QUERY,
        },
    )
    assert response.json()["data"]["me"] == {
        "__typename": "User",
        "id": str(user.id),
        "username": user.username,
    }

import uuid

import pytest
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import User

MUTATION = """
mutation UserCreate($input: UserCreateInput!) {
  userCreate(input: $input) {
    __typename
    ... on UserCreateSuccess {
      user {
        id
        username
      }
    }
    ... on Error {
      __typename
      message
    }
  }
}
"""


@pytest.mark.parametrize(
    "username,password",
    [
        ("Username", "123"),
        ("12", "Password"),
    ],
)
async def test_validation_errors(
    http_client: AsyncClient,
    session: AsyncSession,
    username: str,
    password: str,
):
    response = await http_client.post(
        "/graphql/",
        json={
            "query": MUTATION,
            "variables": {
                "input": {
                    "username": username,
                    "password": password,
                }
            },
        },
    )
    data = response.json()["data"]
    assert data["userCreate"]["__typename"] == "ValidationErrors"


async def test_create_user(http_client: AsyncClient, session: AsyncSession):
    response = await http_client.post(
        "/graphql/",
        json={
            "query": MUTATION,
            "variables": {
                "input": {
                    "username": "Username",
                    "password": "Password",
                }
            },
        },
    )
    data = response.json()["data"]
    assert data["userCreate"]["__typename"] == "UserCreateSuccess"
    user_id = data["userCreate"]["user"]["id"]
    user = await session.get(User, user_id)
    assert isinstance(user, User)
    assert user.username == data["userCreate"]["user"]["username"]


async def test_taken_username(
    http_client: AsyncClient,
    session: AsyncSession,
):
    username = str(uuid.uuid4())
    session.add(User(username=username, password_hash=""))
    await session.commit()

    response = await http_client.post(
        "/graphql/",
        json={
            "query": MUTATION,
            "variables": {
                "input": {
                    "username": username,
                    "password": "Password",
                }
            },
        },
    )
    data = response.json()["data"]
    assert data["userCreate"]["__typename"] == "UsernameTakenError"

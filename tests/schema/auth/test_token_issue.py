import uuid

import pytest
from httpx import AsyncClient

from core.services import UserService
from db.models import User

MUTATION = """
mutation TokenIssue($input: TokenIssueInput!) {
  tokenIssue(input: $input) {
    __typename
    ... on Tokens {
      accessToken
    }
    ... on Error {
      message
    }
  }
}
"""


@pytest.fixture
def password() -> str:
    return str(uuid.uuid4())


@pytest.fixture
async def user(user_service: UserService, password: str) -> User:
    return await user_service.user_create(
        username=str(uuid.uuid4()),
        password=password,
    )


async def test_issue_token(
    http_client: AsyncClient,
    user: User,
    password: str,
):
    response = await http_client.post(
        "/graphql/",
        json={
            "query": MUTATION,
            "variables": {
                "input": {
                    "username": user.username,
                    "password": password,
                }
            },
        },
    )
    assert response.json()["data"]["tokenIssue"]["__typename"] == "Tokens"


async def test_invalid_credentials(
    http_client: AsyncClient,
    user: User,
):
    response = await http_client.post(
        "/graphql/",
        json={
            "query": MUTATION,
            "variables": {
                "input": {
                    "username": user.username,
                    "password": str(uuid.uuid4()),
                }
            },
        },
    )
    data = response.json()["data"]
    assert data["tokenIssue"]["__typename"] == "InvalidCredentialsError"

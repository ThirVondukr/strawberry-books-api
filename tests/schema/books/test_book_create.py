import pytest
from httpx import AsyncClient
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import Book

MUTATION = """
mutation BookCreate($input: BookCreateInput!) {
  bookCreate(input: $input) {
    __typename
    ... on Book {
      title
      isbn13
    }
  }
}
"""


async def test_should_be_authenticated(
    http_client: AsyncClient,
    session: AsyncSession,
):
    response = await http_client.post(
        "/graphql/",
        json={
            "query": MUTATION,
            "variables": {
                "input": {
                    "title": "Book Title",
                    "isbn13": "123-4-56-789123-1",
                }
            },
        },
    )
    assert response.json()["data"] is None
    assert response.json()["errors"]
    assert list(await session.scalars(select(Book))) == []


async def test_base_case(
    user_http_client: AsyncClient,
    session: AsyncSession,
):
    response = await user_http_client.post(
        "/graphql/",
        json={
            "query": MUTATION,
            "variables": {
                "input": {
                    "title": "Book Title",
                    "isbn13": "1234567891231",
                }
            },
        },
    )
    assert response.json().get("errors") is None
    assert response.json()["data"]["bookCreate"]["__typename"] == "Book"
    books_in_db = list(await session.scalars(select(Book)))
    assert len(books_in_db) == 1


@pytest.mark.parametrize(
    "title,isbn13",
    [
        (" " * (250 + 1), "1234567891231"),
        ("Title", "1234567891232"),
    ],
)
async def test_validation_errors(
    user_http_client: AsyncClient,
    title: str,
    isbn13: str,
):
    response = await user_http_client.post(
        "/graphql/",
        json={
            "query": MUTATION,
            "variables": {
                "input": {
                    "title": title,
                    "isbn13": isbn13,
                }
            },
        },
    )
    data = response.json()["data"]
    assert data["bookCreate"]["__typename"] == "ValidationErrors"

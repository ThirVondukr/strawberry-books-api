import uuid

import pytest
from fastapi import FastAPI
from httpx import AsyncClient

from core.services import AuthService, UserService
from db.models import User
from settings import AuthSettings


@pytest.fixture
async def password():
    return str(uuid.uuid4())


@pytest.fixture
async def user(user_service: UserService, password: str) -> User:
    return await user_service.user_create(
        username=str(uuid.uuid4()), password=password
    )


@pytest.fixture
async def user_http_client(
    fastapi_app: FastAPI,
    auth_service: AuthService,
    user: User,
    auth_settings: AuthSettings,
) -> AsyncClient:
    access_token = await auth_service.issue_token(user)

    async with AsyncClient(
        app=fastapi_app,
        base_url="http://test",
        headers={
            auth_settings.access_token_header: access_token,
        },
    ) as client:
        yield client

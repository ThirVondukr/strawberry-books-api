import dotenv

dotenv.load_dotenv(".env")
import asyncio

import pytest
from fastapi import FastAPI
from httpx import AsyncClient

from container import create_container

pytest_plugins = [
    "conftest_auth",
    "conftest_db",
    "conftest_services",
]


@pytest.fixture(scope="session")
def event_loop():
    policy = asyncio.get_event_loop_policy()
    loop = policy.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def fastapi_app() -> FastAPI:
    from app import create_app

    return create_app()


@pytest.fixture(scope="session")
async def http_client(fastapi_app: FastAPI) -> AsyncClient:
    async with AsyncClient(app=fastapi_app, base_url="http://test") as client:
        yield client


@pytest.fixture(scope="session")
async def container():
    return create_container()

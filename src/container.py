import functools

from aioinject import Callable, Container, Object, Singleton
from passlib.context import CryptContext

from core.services import AuthService, UserService
from db.dependencies import get_session
from settings import AuthSettings


@functools.lru_cache(maxsize=1)
def create_container() -> Container:
    container = Container()
    container.register(Singleton(AuthSettings))
    container.register(Callable(get_session))
    container.register(Callable(UserService))
    container.register(Callable(AuthService))
    container.register(Object(CryptContext(schemes=["argon2"])))
    return container

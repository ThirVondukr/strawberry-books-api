import time
from typing import Annotated

import jwt
from aioinject import Inject
from passlib.context import CryptContext
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import User
from settings import AuthSettings


class UserService:
    def __init__(
        self,
        session: Annotated[AsyncSession, Inject],
        crypt_context: Annotated[CryptContext, Inject],
    ):
        self.session = session
        self.crypt_context = crypt_context

    async def user_create(
        self,
        username: str,
        password: str,
    ) -> User:
        user = User(
            username=username,
            password_hash=self.crypt_context.hash(password),
        )
        self.session.add(user)
        await self.session.commit()
        await self.session.refresh(user)
        return user

    async def username_taken(self, username: str) -> bool:
        return await self.get_by_username(username) is not None

    async def get_by_username(self, username: str) -> User | None:
        stmt = select(User).filter(User.username == username)
        user: User | None = await self.session.scalar(stmt)
        return user


class AuthService:
    def __init__(
        self,
        session: Annotated[AsyncSession, Inject],
        user_service: Annotated[UserService, Inject],
        crypt_context: Annotated[CryptContext, Inject],
        settings: Annotated[AuthSettings, Inject],
    ):
        self.session = session
        self.user_service = user_service
        self.crypt_context = crypt_context
        self.settings = settings

    async def authenticate(self, username: str, password: str) -> User | None:
        user = await self.user_service.get_by_username(username)
        if user is None:
            return None

        if not self.crypt_context.verify(password, user.password_hash):
            return None

        return user

    async def issue_token(self, user: User) -> str:
        timestamp = int(time.time())
        token_lifetime_seconds = (
            self.settings.access_token_lifetime.total_seconds()
        )
        payload = {
            "sub": str(user.id),
            "iat": timestamp,
            "exp": timestamp + token_lifetime_seconds,
        }
        token = jwt.encode(
            payload=payload,
            key=self.settings.secret_key,
            algorithm=self.settings.algorithm,
        )
        return token

    async def authenticate_token(self, token: str) -> User | None:
        try:
            payload = jwt.decode(
                token,
                key=self.settings.secret_key,
                algorithms=[self.settings.algorithm],
            )
        except jwt.PyJWTError:
            return None

        user: User | None = await self.session.get(User, ident=payload["sub"])
        return user

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from gql.app import graphql_app


def create_app() -> FastAPI:
    app = FastAPI()
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    app.mount("/graphql/", graphql_app)
    return app

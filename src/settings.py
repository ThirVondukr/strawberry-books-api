from datetime import timedelta

from pydantic import BaseSettings


class AuthSettings(BaseSettings):
    class Config:
        env_prefix = "auth_"

    secret_key: str
    algorithm: str = "HS256"

    access_token_lifetime = timedelta(minutes=15)
    access_token_header: str = "access-token"

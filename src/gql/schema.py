from aioinject.ext.strawberry import make_container_ext
from strawberry import Schema
from strawberry.tools import merge_types

from container import create_container

from .extensions import AuthExtension, ContextExtension
from .modules.auth.mutation import AuthMutation
from .modules.books.mutation import BookMutation
from .modules.users.query import UserQuery

Query = merge_types("Query", (UserQuery,))
Mutation = merge_types(
    "Mutation",
    (
        AuthMutation,
        BookMutation,
    ),
)

schema = Schema(
    query=Query,
    mutation=Mutation,
    extensions=[
        make_container_ext(create_container()),
        ContextExtension,
        AuthExtension,
    ],
)

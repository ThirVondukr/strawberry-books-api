from typing import Generic, Iterable, List, Type, TypeVar

TModel = TypeVar("TModel")
TType = TypeVar("TType")


class OrmTypeMixin(Generic[TModel]):
    """
    Helper class to convert models into strawberry types
    """

    @classmethod
    def from_orm(cls: Type[TType], model: TModel) -> TType:
        raise NotImplementedError

    @classmethod
    def from_orm_optional(
        cls: Type[TType], model: TModel | None
    ) -> TType | None:
        if model is None:
            return None
        return cls.from_orm(model)  # type: ignore

    @classmethod
    def from_orm_list(
        cls: Type[TType], models: Iterable[TModel]
    ) -> List[TType]:
        return [cls.from_orm(model) for model in models]  # type: ignore

import dataclasses

from starlette.requests import Request
from starlette.responses import Response
from starlette.websockets import WebSocket
from strawberry.types import Info as StrawberryInfo

from db.models import User


@dataclasses.dataclass
class Context:
    request: Request | WebSocket
    response: Response | None
    maybe_user: User | None

    @property
    def user(self) -> User:
        if self.maybe_user is None:
            raise ValueError
        return self.maybe_user


@dataclasses.dataclass
class Info(StrawberryInfo[Context, None]):
    pass

import strawberry

from gql.errors import ValidationErrors

from .types import BookType

BookCreateResult = strawberry.union(
    "BookCreateResult",
    (BookType, ValidationErrors),
)

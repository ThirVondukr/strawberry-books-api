from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject
from sqlalchemy.ext.asyncio import AsyncSession

from db.models import Book
from gql.modules.auth.permissions import IsAuthenticated
from gql.validation import validate

from .inputs import BookCreateInput
from .results import BookCreateResult
from .types import BookType


@strawberry.type
class BookMutation:
    @strawberry.mutation(permission_classes=[IsAuthenticated])
    @inject
    async def book_create(
        self, input: BookCreateInput, session: Annotated[AsyncSession, Inject]
    ) -> BookCreateResult:

        if errors := validate(input):
            return errors

        book = Book(
            isbn13=input.isbn13,
            title=input.title,
        )

        session.add(book)
        await session.commit()
        await session.refresh(book)

        return BookType.from_orm(book)

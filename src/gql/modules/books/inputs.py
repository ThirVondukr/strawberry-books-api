import strawberry
from pydantic import BaseModel, Field, validator


def validate_isbn_13(isbn: str) -> str:
    """
    Basic isbn13 checksum validation
    """
    isbn_numbers = list(map(int, isbn))
    checksum = sum(isbn_numbers[::2])
    checksum += sum(isbn_numbers[1::2]) * 3

    if checksum % 10 != 0:
        raise ValueError(f'"{isbn}" is not valid a isbn')
    return isbn


class BookCreateInputModel(BaseModel):
    isbn13: str = Field(min_length=13, max_length=13)

    title: str = Field(min_length=1, max_length=250)

    @validator("isbn13")
    def validate_isbn13(cls, value: str) -> str:
        return validate_isbn_13(value)


@strawberry.experimental.pydantic.input(BookCreateInputModel)  # type: ignore
class BookCreateInput:
    isbn13: strawberry.auto
    title: strawberry.auto

import strawberry

from db.models import Book
from gql.mixins import OrmTypeMixin


@strawberry.type(name="Book")
class BookType(OrmTypeMixin[Book]):
    isbn13: str
    title: str

    @strawberry.field
    def isbn10(self) -> str | None:
        if not self.isbn13.startswith("978"):
            return None
        return self.isbn13[3:]

    @classmethod
    def from_orm(cls, model: Book) -> "BookType":
        return cls(
            isbn13=model.isbn13,
            title=model.title,
        )

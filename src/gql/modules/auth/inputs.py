import strawberry
from pydantic import BaseModel, Field


class UserCreateInputModel(BaseModel):
    username: str = Field(min_length=3, max_length=100)
    password: str = Field(min_length=4, max_length=250)


@strawberry.experimental.pydantic.input(UserCreateInputModel)  # type: ignore
class UserCreateInput:
    username: strawberry.auto
    password: strawberry.auto


@strawberry.input
class TokenIssueInput:
    username: str
    password: str

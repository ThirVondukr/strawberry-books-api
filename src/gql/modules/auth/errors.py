import strawberry

from gql.errors import Error


@strawberry.type
class UsernameTakenError(Error):
    pass


@strawberry.type
class InvalidCredentialsError(Error):
    pass

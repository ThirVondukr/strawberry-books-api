from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject

from core.services import AuthService, UserService
from gql.modules.auth.errors import InvalidCredentialsError, UsernameTakenError
from gql.modules.auth.inputs import TokenIssueInput, UserCreateInput
from gql.modules.auth.results import (
    TokenIssueResult,
    UserCreateResult,
    UserCreateSuccess,
)
from gql.modules.auth.types import Tokens
from gql.modules.users.types import UserType
from gql.validation import validate


@strawberry.type
class AuthMutation:
    @strawberry.mutation
    @inject
    async def user_create(
        self,
        input: UserCreateInput,
        user_service: Annotated[UserService, Inject],
    ) -> UserCreateResult:
        if errors := validate(input):
            return errors

        if await user_service.username_taken(input.username):
            return UsernameTakenError(
                message=f"Username {input.username} is taken"
            )

        user = await user_service.user_create(
            username=input.username,
            password=input.password,
        )
        return UserCreateSuccess(user=UserType.from_orm(user))

    @strawberry.mutation
    @inject
    async def token_issue(
        self,
        input: TokenIssueInput,
        auth_service: Annotated[AuthService, Inject],
    ) -> TokenIssueResult:
        user = await auth_service.authenticate(
            username=input.username,
            password=input.password,
        )
        if user is None:
            return InvalidCredentialsError(
                message="Invalid username or password"
            )
        access_token = await auth_service.issue_token(user)
        return Tokens(
            access_token=access_token,
        )

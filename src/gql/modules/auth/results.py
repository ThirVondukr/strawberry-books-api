import strawberry

from gql.errors import ValidationErrors
from gql.modules.auth.errors import InvalidCredentialsError, UsernameTakenError
from gql.modules.auth.types import Tokens
from gql.modules.users.types import UserType


@strawberry.type
class UserCreateSuccess:
    user: UserType


UserCreateResult = strawberry.union(
    "UserCreateResult",
    (UserCreateSuccess, UsernameTakenError, ValidationErrors),
)

TokenIssueResult = strawberry.union(
    "TokenIssueResult",
    (Tokens, InvalidCredentialsError),
)

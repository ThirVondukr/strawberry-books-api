from typing import Any

from strawberry import BasePermission

from gql.context import Info


class IsAuthenticated(BasePermission):
    def has_permission(  # type: ignore
        self,
        source: Any,
        info: Info,
        **kwargs: Any,
    ) -> bool:
        return info.context.maybe_user is not None

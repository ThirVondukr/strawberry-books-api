import strawberry

from gql.context import Info

from .types import UserType


@strawberry.type
class UserQuery:
    @strawberry.field
    def me(self, info: Info) -> UserType | None:
        return UserType.from_orm_optional(info.context.maybe_user)

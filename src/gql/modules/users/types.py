import uuid

import strawberry

from db.models import User
from gql.mixins import OrmTypeMixin


@strawberry.type(name="User")
class UserType(OrmTypeMixin[User]):
    id: uuid.UUID
    username: str

    @classmethod
    def from_orm(cls, model: User) -> "UserType":
        return cls(
            id=model.id,
            username=model.username,
        )

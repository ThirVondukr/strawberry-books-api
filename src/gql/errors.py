import strawberry


@strawberry.interface
class Error:
    message: str


@strawberry.type
class ValidationError(Error):
    code: str
    message: str
    location: list[str]


@strawberry.type
class ValidationErrors(Error):
    errors: list[ValidationError]

from typing import Annotated

from aioinject import Inject
from aioinject.ext.strawberry import inject
from graphql.pyutils import AwaitableOrValue
from starlette.websockets import WebSocket
from strawberry.extensions import Extension

from core.services import AuthService

from .context import Context


class ContextExtension(Extension):
    def on_request_start(self) -> AwaitableOrValue[None]:
        self.execution_context.context = Context(
            request=self.execution_context.context["request"],
            response=self.execution_context.context["response"],
            maybe_user=None,
        )
        return None


class AuthExtension(Extension):
    @inject
    async def on_request_start(
        self,
        auth_service: Annotated[AuthService, Inject],
    ) -> AwaitableOrValue[None]:
        request = self.execution_context.context.request
        if isinstance(request, WebSocket):
            return None

        user = await auth_service.authenticate_token(
            request.headers.get(auth_service.settings.access_token_header, "")
        )
        self.execution_context.context.maybe_user = user

        return None

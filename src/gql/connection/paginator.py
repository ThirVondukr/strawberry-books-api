import base64
import enum
from enum import Enum
from typing import Any, Optional, TypeVar

from sqlalchemy import tuple_
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import DeclarativeMeta, InstrumentedAttribute
from sqlalchemy.sql import Select

from gql.connection import Edge, PageInfo


def model_fields_enum(
    *columns: InstrumentedAttribute, name: str
) -> type[enum.Enum]:
    return enum.Enum(  # type: ignore
        name,
        names={column.name.upper(): column.prop for column in columns},
        module=__name__,
    )


def encode_cursor(
    model: DeclarativeMeta,
    order_by: list[InstrumentedAttribute],
) -> str:
    values = [
        str(getattr(model, attr.prop.class_attribute.name)) for attr in order_by
    ]
    return ":".join(
        base64.b64encode(value.encode()).decode() for value in values
    )


def decode_cursor(
    cursor: str,
    order_by: list[InstrumentedAttribute],
) -> list[Any]:
    return [
        base64.b64decode(value.encode()).decode() for value in cursor.split(":")
    ]


TEdge = TypeVar("TEdge")


async def paginate(
    query: Select,
    order_by: list[Enum] | list[InstrumentedAttribute],
    after: str,
    before: str,
    first: Optional[int],
    last: Optional[int],
    session: AsyncSession,
) -> tuple[list[Edge[Any]], PageInfo]:
    order_by = [
        col
        if isinstance(col, InstrumentedAttribute)
        else col.value.class_attribute
        for col in order_by
    ]
    if first and first < 0:
        raise ValueError

    if last and last < 0:
        raise ValueError

    if first and last:
        raise ValueError

    order_clause = tuple_(*order_by)
    if last:
        order_clause = order_clause.desc()
    query = query.order_by(order_clause)
    limit: int = (first or last) + 1  # type: ignore
    query = query.limit(limit)

    if after:
        query = query.filter(decode_cursor(after, order_by) < order_by)
    if before:
        query = query.filter(order_by < decode_cursor(before, order_by))

    nodes = list(await session.scalars(query))

    page_info = PageInfo(
        has_previous_page=False,
        has_next_page=False,
        start_cursor="",
        end_cursor="",
    )
    if first and len(nodes) > first:
        page_info.has_next_page = True
        nodes = nodes[:first]

    if last:
        # We need to reverse nodes since we used order_by.desc()
        nodes.reverse()
        if len(nodes) > last:
            page_info.has_previous_page = True
            nodes = nodes[-last:]

    if nodes:
        page_info.start_cursor = encode_cursor(nodes[0], order_by)
        page_info.end_cursor = encode_cursor(nodes[-1], order_by)
    edges = [
        Edge(node=node, cursor=encode_cursor(node, order_by)) for node in nodes
    ]
    return edges, page_info

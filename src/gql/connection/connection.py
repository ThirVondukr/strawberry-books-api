from typing import Generic, TypeVar

import strawberry

TNode = TypeVar("TNode")
TEdge = TypeVar("TEdge")


@strawberry.type
class PageInfo:
    has_previous_page: bool
    has_next_page: bool
    start_cursor: str
    end_cursor: str


@strawberry.type
class Edge(Generic[TNode]):
    node: TNode
    cursor: str


@strawberry.type
class Connection(Generic[TEdge]):
    edges: list[TEdge]
    page_info: PageInfo

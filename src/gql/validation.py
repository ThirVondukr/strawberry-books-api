from typing import Any

import pydantic

from .errors import ValidationError, ValidationErrors


def validate(type_: Any) -> ValidationErrors | None:
    if not hasattr(type_, "_type_definition"):
        raise ValueError
    try:
        type_.to_pydantic()
    except pydantic.ValidationError as exc:
        errors = [
            ValidationError(
                location=list(map(str, error["loc"])),
                message=error["msg"],
                code=error["type"],
            )
            for error in exc.errors()
        ]
        return ValidationErrors(errors=errors, message="Validation Error")
    return None

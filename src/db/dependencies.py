import contextlib
from typing import AsyncGenerator

from sqlalchemy.ext.asyncio import AsyncSession

from db.base import async_sessionmaker


@contextlib.asynccontextmanager
async def get_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_sessionmaker() as session:
        yield session

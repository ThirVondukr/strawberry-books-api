import uuid

from sqlalchemy import Column, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import Mapped

from db.base import Base


class User(Base):
    __tablename__ = "user"

    id: Mapped[uuid.UUID] = Column(
        UUID(as_uuid=True), primary_key=True, default=uuid.uuid4
    )
    username: Mapped[str] = Column(String(100), unique=True, nullable=False)
    password_hash: Mapped[str] = Column(String(255), nullable=False)


class Book(Base):
    __tablename__ = "book"

    isbn13: Mapped[str] = Column(String(13), primary_key=True)

    title: Mapped[str] = Column(String(250), nullable=False)

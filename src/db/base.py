import os

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import declarative_base, sessionmaker

async_engine = create_async_engine(
    os.getenv("DB_URL", ""),
    future=True,
)
async_sessionmaker = sessionmaker(
    bind=async_engine,
    future=True,
    class_=AsyncSession,
)
Base = declarative_base()
